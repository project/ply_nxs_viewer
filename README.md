PLY NXS Viewer

About this module
============

The PLY NXS Viewer module provides a new configurable
file field formatter called "3D Model(s)". This formatter 
use the 3DHOP JavaScript library to render 3D Objects 
(.NXS and .PLY files). See the 3DHOP library website 
(http://3dhop.net/index.php) for more information 
and examples.

How to use ?
============

* Please first download the version 4.0.3 of the 3DHop 
  library [on this page](https://github.com/cnr-isti-vclab/3DHOP/releases), 
  and place it in the libraries directory.
* In any content type, create a new `File` field. 
  This field must be a multiple file field, 
  with a maximum of 3 files. Only .ply and .nxs extensions 
  must be allowed.
* In the `Manage display` options, choose `3D Model(s)` 
  as Format type for this new field.
* You can now choose (in the formatter configuration) 
  the Viewer Options to be displayed.

Credits
============

Contributors
------------

* Romain Moro : romain.moro@makina-corpus.com
* Twitter: : https://twitter.com/c4ilus

Companies
---------

* Makina Corpus : http://makina-corpus.com/expertise/expertise-drupal
* Contact us at mailto:drupal@makina-corpus.com
