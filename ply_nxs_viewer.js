/**
 * @file
 * PLY NXS Viewer related javascript.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.plynxsviewer_hop = {
    attach: function (context, settings) {
      init3dhop();
      setup3dhop();

      var canvas_width = $('#mainviewercontainer').width();
      var canvas_height = $('#mainviewercontainer').height();

      resizeCanvas(canvas_width, canvas_height);
      moveToolbar(0, 60);
      moveMeasurebox(10, 10);

      $('a[href$="ply"]').hide();
      $('a[href$="nxs"]').hide();

      $('#colorpicker').change(function () {
        $('#draw-canvas').css('background', $(this).val());
        $('.tdhop').css('background', $(this).val());
        $('.choosemode').css('background', $(this).val());
        $('#mainviewercontainer').css('background', $(this).val());
      });

      $('#3dhop').ready(function () {
        $('#trackballspecimen').click();
      });

      $('.applytransparencybtn').click(function () {
        var valeurmodele = $('#modeletransparency option:checked').val();
        var valeuropacite = $('#valuetransparency option:checked').val();
        presenter.setInstanceTransparencyByName(valeurmodele, true, true, valeuropacite);
      });

      $('.disabletransparencybtn').click(function () {
        presenter.setInstanceTransparencyByName('Model1', false, true, 1);
        presenter.setInstanceTransparencyByName('Model2', false, true, 1);
        presenter.setInstanceTransparencyByName('Model3', false, true, 1);
      });
    }
  };
}(jQuery));
