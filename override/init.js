/*
 3DHOP - 3D Heritage Online Presenter
 Copyright (c) 2014, Marco Callieri - Visual Computing Lab, ISTI - CNR
 All rights reserved.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint no-unused-vars: 0 */

function onEndMeasure(measure) {
  'use strict';
  jQuery('#measure-output').html(Math.round(measure * 1000) / (1000));
}

var presenter = null;

function setup3dhop() {
  'use strict';
  presenter = new Presenter('draw-canvas');

  // Array() to select all nxs or ply files links
  var arrayurl = [];

  // Firstly, we try NXS files
  jQuery("a[href$='nxs']").each(function (index) {
    arrayurl.push(jQuery(this).attr('href'));
  });

  // If no NXS files, we test PLY
  if (arrayurl.length === 0) {
    jQuery("a[href$='ply']").each(function (index) {
      arrayurl.push(jQuery(this).attr('href'));
    });
  }

  // Maximum 5 files by page (files may be large and impact the page performance)
  presenter.setScene({
    meshes: {
      Modele0: {url: arrayurl[0]},
      Modele1: {url: arrayurl[1]},
      Modele2: {url: arrayurl[2]},
      Modele3: {url: arrayurl[3]},
      Modele4: {url: arrayurl[4]}
    },

    modelInstances: {
      Model0: {mesh: 'Modele0'},
      Model1: {mesh: 'Modele1'},
      Model2: {mesh: 'Modele2'},
      Model3: {mesh: 'Modele3'},
      Model4: {mesh: 'Modele4'}
    }
  });

  presenter._onEndMeasurement = onEndMeasure;
}

function actionsToolbar(action) {
  'use strict';
  if (action === 'home') {
    presenter.resetTrackball();
  }
  else if (action === 'zoomin') {
    presenter.zoomIn();
  }
  else if (action === 'zoomout') {
    presenter.zoomOut();
  }
  else if (action === 'light' || action === 'light_on') {
    presenter.enableLightTrackball(!presenter.isLightTrackballEnabled());
    lightSwitch();
  }
  else if (action === 'measure' || action === 'measure_on') {
    presenter.enableMeasurementTool(!presenter.isMeasurementToolEnabled());
    measurementSwitch();
  }
  else if (action === 'full' || action === 'full_on') {
    fullscreenSwitch();
  }
}

function init3dhop() {
  'use strict';
  var interval;
  var id;
  var ismousedown;
  var button = 0;

  jQuery('#toolbar3D img')
    .mouseenter(function (e) {
      id = jQuery(this).attr('id');
      if (!ismousedown) {
        jQuery(this).css('opacity', '0.8');
      }
      else {
        jQuery(this).css('opacity', '1.0');
      }
    })
    .mouseout(function (e) {
      clearInterval(interval);
      jQuery(this).css('opacity', '0.5');
    })
    .mousedown(function (e) {
      ismousedown = true;
      if (e.button === button) {
        actionsToolbar(id);
        if (id === 'zoomin' || id === 'zoomout') {
          interval = setInterval(function () {
            actionsToolbar(id);
          }, 100);
        }
        else {
          clearInterval(interval);
        }
        jQuery(this).css('opacity', '1.0');
        button = 0;
      }
    })
    .mouseup(function (e) {
      ismousedown = false;
      if (e.button === button) {
        clearInterval(interval);
        jQuery(this).css('opacity', '0.8');
        button = 0;
      }
    })
    .on('touchstart', function (e) {
      button = 2;
    })
    .on('touchend', function (e) {
      button = 0;
    });

  jQuery('#measure-output')
    .on('contextmenu', function (e) {
      e.stopPropagation();
    });

  jQuery('#3dhop')
    .on('contextmenu', function (e) {
      return false;
    })
    .on('touchstart', function (e) {
      jQuery('#toolbar3D img').css('opacity', '0.5');
    })
    .on('touchend', function (e) {
      clearInterval(interval);
    })
    .on('touchmove', function (e) {
      clearInterval(interval);
      jQuery('#toolbar3D img').css('opacity', '0.5');
    });

  jQuery('#draw-canvas')
    .mousedown(function () {
      jQuery('#toolbar3D img').css('opacity', '0.5');
    });

  if (window.navigator.userAgent.indexOf('Trident/') > 0) {
    jQuery('#full').click(function (e) {
      enterFullscreen();
    });
    jQuery('#full_on').click(function (e) {
      exitFullscreen();
    });
  }

  resizeCanvas(jQuery('#3dhop').parent().width(), jQuery('#3dhop').parent().height());

  set3dhlg();
}

function lightSwitch() {
  'use strict';
  var on = presenter.isLightTrackballEnabled();

  if (on) {
    jQuery('#light').css('visibility', 'hidden');
    jQuery('#light_on').css('visibility', 'visible');
    jQuery('#light_on').css('opacity', '1.0');
    // jQuery('#draw-canvas').css("cursor","url(./skins/icons/cursor_light.png), auto");
  }
  else {
    jQuery('#light_on').css('visibility', 'hidden');
    jQuery('#light').css('visibility', 'visible');
    jQuery('#light').css('opacity', '1.0');
    // jQuery('#draw-canvas').css("cursor","default");
  }
}

function measurementSwitch() {
  'use strict';
  var on = presenter.isMeasurementToolEnabled();

  if (on) {
    jQuery('#measure').css('visibility', 'hidden');
    jQuery('#measure_on').css('visibility', 'visible');
    jQuery('#measure_on').css('opacity', '1.0');
    jQuery('#measurebox').css('visibility', 'visible');
    jQuery('#draw-canvas').css('cursor', 'crosshair');
  }
  else {
    jQuery('#measure_on').css('visibility', 'hidden');
    jQuery('#measure').css('visibility', 'visible');
    jQuery('#measure').css('opacity', '1.0');
    jQuery('#measurebox').css('visibility', 'hidden');
    jQuery('#measure-output').html('0.0');
    jQuery('#draw-canvas').css('cursor', 'default');
  }
}

function fullscreenSwitch() {
  'use strict';
  if (jQuery('#full').css('visibility') === 'visible') {
    if (window.navigator.userAgent.indexOf('Trident/') < 0) {
      enterFullscreen();
    }
  }
  else {
    if (window.navigator.userAgent.indexOf('Trident/') < 0) {
      exitFullscreen();
    }
  }
}

function enterFullscreen() {
  'use strict';
  var el = document.getElementById('3dhop');
  presenter.native_width = presenter.ui.width;
  presenter.native_height = presenter.ui.height;
  jQuery('#full').css('visibility', 'hidden');
  jQuery('#full_on').css('visibility', 'visible');
  jQuery('#full_on').css('opacity', '0.5');
  resizeCanvas(screen.width, screen.height);

  if (el.msRequestFullscreen) {
    el.msRequestFullscreen();
  }
  else if (el.mozRequestFullScreen) {
    el.mozRequestFullScreen();
  }
  else if (el.webkitRequestFullscreen) {
    el.webkitRequestFullscreen();
  }

  presenter.ui.postDrawEvent();
}

function exitFullscreen() {
  'use strict';
  jQuery('#full_on').css('visibility', 'hidden');
  jQuery('#full').css('visibility', 'visible');
  jQuery('#full').css('opacity', '0.5');
  resizeCanvas(presenter.native_width, presenter.native_height);

  if (document.msExitFullscreen) {
    document.msExitFullscreen();
  }
  else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  }
  else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }

  presenter.ui.postDrawEvent();
}

function moveToolbar(l, t) {
  'use strict';
  jQuery('#toolbar3D').css('left', l);
  jQuery('#toolbar3D').css('top', t);
}

function moveMeasurebox(r, t) {
  'use strict';
  jQuery('#measurebox').css('right', r);
  jQuery('#measurebox').css('bottom', t);
}

function resizeCanvas(w, h) {
  'use strict';
  jQuery('#draw-canvas').attr('width', w);
  jQuery('#draw-canvas').attr('height', h);
  jQuery('#3dhop').css('width', w);
  jQuery('#3dhop').css('height', h);
}

function set3dhlg() {
  'use strict';
  jQuery('#tdhlg').html('Powered by 3DHOP</br>&nbsp;C.N.R. &nbsp;&ndash;&nbsp; I.S.T.I.');
  jQuery('#tdhlg').mouseover(function () {
    jQuery('#tdhlg').animate({
      height: '25px'
    }, 'fast');
  }).mouseout(function () {
    jQuery('#tdhlg').animate({
      height: '13px'
    }, 'slow');
  });
  jQuery('#tdhlg').click(function () {
    window.open('http://vcg.isti.cnr.it/3dhop/', '_blank');
  });
}

document.addEventListener('MSFullscreenChange', function () {
  'use strict';
  if (!document.msFullscreenElement) {
    exitFullscreen();
  }
}, false);

document.addEventListener('mozfullscreenchange', function () {
  'use strict';
  if (!document.mozFullScreen) {
    exitFullscreen();
  }
}, false);

document.addEventListener('webkitfullscreenchange', function () {
  'use strict';
  if (!document.webkitIsFullScreen) {
    exitFullscreen();
  }
}, false);
