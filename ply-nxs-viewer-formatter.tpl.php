<?php

/**
 * @file
 * Template file.
 *
 * Variables details :
 *  - $models: array containing informations about 3D objects.
 *  - $opacity: boolean. Allows a user to change the opacity of an object.
 *  - $background_color: boolean. Allows a user to change the
 *    background color of the viewer.
 *  - $toolbar: boolean. Allows a user to use the toolbar.
 *  - $zoom: boolean. Allows a user to zoom on objects.
 *  - $measure_tool. boolean. Allows a user to use the measurement tool.
 *  - $full_screen. boolean. Allows a user to switch on/off
 *    "full screen" mode.
 *  - $horizontal_axis_x: integer. Horizontal shift (in pixels)
 *    from the left-up corner of the Canvas.
 *  - $vertical_axis_y: integer. Vertical shift (in pixels)
 *    from the left-up corner of the Canvas.
 *  - $depth_z: integer. Depth of the visualization.
 *  - $skin: string. Path to the actual chosen viewer theme.
 */
?>

<div id="mainviewercontainer">
  <div id="3dhop" class="tdhop">
    <div class="choosemode">
      <?php
      // Models selector.
      for ($i = 0; $i < count($models); $i++) {
        echo '<input checked="checked" type="checkbox" onclick="presenter.toggleInstanceVisibilityByName(\'Model' . $i . '\', true);" /> ' . $models[$i]['name'];
      }

      echo "<br />";

      // Opacity setting.
      if ($opacity == 1) {
        echo t('Opacity');

        // Which model ?
        echo '<select id="modeletransparency">';
        for ($i = 0; $i < count($models); $i++) {
          echo '<option value="Model' . $i . '">' . $models[$i]['name'] . '</option>';
        }
        echo ' </select>';

        // Opacity value selection.
        echo '<select id="valuetransparency">';
        for ($i = 1; $i < 11; $i++) {
          echo '<option value="' . $i / 10 . '">' . $i / 10 . '</option>';
        }
        echo '</select>';

        ?>

        <input type="button" class="applytransparencybtn"
               value="<?php echo t('Apply') ?>"/>
        <input type="button" class="disabletransparencybtn"
               value="<?php echo t('Cancel') ?>"/>

        <?php
      }

      // Background color setting.
      if ($background_color == 1) { ?>

        <select id="colorpicker">
          <option value="#fff"><?php echo t('White') ?></option>
          <option value="#7bd148"><?php echo t('Green') ?></option>
          <option value="#a4bdfc"><?php echo t('Blue') ?></option>
          <option value="#ffb878"><?php echo t('Orange') ?></option>
          <option value="#e1e1e1"><?php echo t('Gray') ?></option>
          <option value="#000"><?php echo t('Black') ?></option>
        </select>

      <?php }

      // If toolbar is enabled.
      if ($toolbar == 1) {
        echo '<div id="toolbar3D">';
        echo '<a id="trackballspecimen" onclick="presenter.animateToTrackballPosition([ ' . $horizontal_axis_x . ', ' . $vertical_axis_y . ', ' . $depth_z . ']);"></a>';
        echo '<img id="home" title="Home" src="' . $skin . '/home.png" />';

        if ($zoom == 1) {
          echo '<br /><img id="zoomin" title="Zoom In" src="' . $skin . '/zoomin.png" /><br /><img id="zoomout" title="Zoom Out" src="' . $skin . '/zoomout.png"/>';
        }

        if ($light_control == 1) {
          echo '<br /><img id="light_on" title="Disable Light Control" src="' . $skin . '/light_on.png" style="position:absolute; visibility:hidden;" /><img id="light" title="Enable Light Control" src="' . $skin . '/light.png"/>';
        }

        if ($measure_tool == 1) {
          echo '<br /><img id="measure_on" title="Disable Measure Tool" src="' . $skin . '/measure_on.png" style="position:absolute; visibility:hidden;" /><img id="measure" title="Enable Measure Tool" src="' . $skin . '/measure.png"/>';
        }

        if ($full_screen == 1) {
          echo '<br /><img id="full_on" title="Exit Full Screen" src="' . $skin . '/full_on.png" style="position:absolute; visibility:hidden;" /><img id="full" title="Full Screen" src="' . $skin . '/full.png"/>';
        }

        echo '</div>';
      } ?>

      <div id="measurebox"><?php echo t('Measured length') ?><br/>
        <span id="measure-output"
              onmousedown="event.stopPropagation()">0.0</span> mm
      </div>

      <?php

      for ($i = 0; $i < count($models); $i++) {
        echo '<a style="display:none" href="' . $models[$i]['link'] . '">' . $models[$i]['name'] . '</a>';
      }
      ?>

      <!-- Final canvas -->
      <canvas id="draw-canvas"/>
    </div>
  </div>
</div>
